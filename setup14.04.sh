#!/bin/bash
# Nemo file manager 
sudo add-apt-repository ppa:webupd8team/nemo
sudo apt-get update
# Cmake 3.2
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:george-edison55/cmake-3.x
# Rabbit vcs
sudo add-apt-repository ppa:rabbitvcs/ppa
sudo apt-get update
# gimp
sudo add-apt-repository ppa:otto-kesselgulasch/gimp
sudo apt-get update
# blender
sudo add-apt-repository ppa:irie/blender
sudo apt-get update
# xnView
sudo apt-add-repository ppa:dhor/myway
sudo apt-get update
# qpdfview
sudo apt-add-repository ppa:b-eltzner/qpdfview
sudo apt-get update
# PyCharm
sudo add-apt-repository ppa:mystic-mirage/pycharm

###################################
## primitive tools
# pkg-config
sudo apt-get -yV install pkg-config 
# open in terminal
sudo apt-get -yV install nautilus-open-terminal
# vim
sudo apt-get -yV install vim
# Nemo file manager 
sudo apt-get -yV install nemo nemo-fileroller
# GNU source-highlight
sudo apt-get -yV install source-highlight
export LESS='-R'
export LESSOPEN='| /usr/share/source-highlight/src-hilite-lesspipe.sh %s'

###################################
## Graphics
# image viewer
sudo apt-get -yV install geeqie
# gimp
sudo apt-get -yV install gimp
# Blender
sudo apt-get -yV install blender
# imagemagick
sudo apt-get -yV install imagemagick perlmagick
# xnview
sudo apt-get -yV install xnview
# video
sudo apt-get -yV install vlc browser-plugin-vlc

###################################
## diff tools
# diff viewer
sudo apt-get -yV install meld
# diff for pdf
sudo apt-get -yV install diffpdf

###################################
## for version control
# git, cvs
sudo apt-get -yV install git cvs
# RabbitVCS
sudo apt-get -yV install rabbitvcs-nautilus3 rabbitvcs-gedit nemo-rabbitvcs

###################################
## C++
# building c/c++
sudo apt-get -yV install build-essential
# debugger
sudo apt-get -yV install valgrind 
# Doxygen
sudo apt-get -yV install doxygen doxygen-gui graphviz
# CMake
sudo apt-get -yV install cmake cmake-gui cmake-curses-gui
# Boost
sudo apt-get -yV install libboost-all-dev
# BLAS & LAPACK
sudo apt-get -yV install libblas-dev libatlas-dev liblapack-dev libatlas-base-dev
# OpenGL
sudo apt-get -yV install freeglut3-dev
sudo apt-get -yV install libglew1.5-dev libglm-dev
# imagemagick
sudo apt-get -yV install libmagick++-dev libmagickcore-dev libmagickwand-dev
# CImg
sudo apt-get -yV install cimg-dev cimg-doc cimg-examples
# TBB
sudo apt-get -yV install libtbb-dev
# SuiteSparse
sudo apt-get -yV install libsuitesparse-dev
# gnuplot
sudo apt-get -yV install gnuplot
# pre-requisities for ceres-solver
sudo apt-get -yV install python-sphinx
# pre-requisities for mlpack
sudo apt-get -yV install libxml2-dev txt2man
# pre-requisities for libDAI
sudo apt-get -yV install libgmp-dev swig
# pre-requisities for OpenGM
sudo apt-get -yV install libhdf5-serial-dev
# pre-requisities for OpenCV
sudo apt-get -yV install libvtk5-qt4-dev libqt4-dev libqt4-opengl-dev libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant libqt5-*
# Blitz++ for arrays computation
sudo apt-get -yV install libblitz-doc libblitz0-dev libblitz0ldbl
# 
sudo apt-get -yV install gfortran

## XML parser for C++
# TinyXML2
sudo apt-get -yV install libtinyxml-dev
# for xml
sudo apt-get -yV install libxerces-c-dev xsdcxx

## Python
sudo apt-get -yV install pycharm-community

###################################
## etc.
# pdfimages
sudo apt-get -yV install poppler-utils
# qpdfview
sudo apt-get -yV install qpdfview
# to avoid mojibake in adobe flash
sudo apt-get -yV install fonts-arphic-uming
fc-cache -fv
sudo cp /usr/share/fonts/truetype/takao-gothic/TakaoPGothic.ttf /usr/share/fonts/truetype/arphic/uming.ttc
# set nemo as default 
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search
gsettings set org.gnome.desktop.background show-desktop-icons false
gsettings set org.nemo.desktop show-desktop-icons true
sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/nemo-autostart.desktop
# mozc
sudo apt-get -yV install ibus-mozc
# filezilla 
sudo apt-get -yV install filezilla

# TeXLive
sudo apt-get -yV install texlive-full texlive-fonts-recommended texlive-science texlive-lang-cjk texlive-bibtex-extra biber

# TexStudio
sudo apt-add-repository ppa:blahota/texstudio
sudo apt-get update
sudo apt-get -yV install texstudio

# JabRef
sudo apt-get -yV install jabref

###################################
## using dpkg
cd
cd Documents
mkdir software
dirSoft="./Documents/software"

# Sublime text
# cd
# cd ${dirSoft}
# mkdir sublime
# cd sublime
# sublimedeb="sublime-text_build-3083_amd64.deb"
# wget http://c758482.r82.cf2.rackcdn.com/${sublimedeb}
# sudo dpkg -i ${sublimedeb} 

# Qt creator
cd 
cd ${dirSoft}
mkdir qtcreator
cd qtcreator
# Qt OpenGL
sudo apt-get -yV install libfontconfig1-dev libfreetype6-dev libx11-dev libxext-dev libxfixes-dev libxi-dev libxrender-dev libxcb1-dev libx11-xcb-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev libxcb-icccm4-dev libxcb-sync0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0-dev
sudo apt-get -yV install libxcb1 libxcb1-dev libx11-xcb1 libx11-xcb-dev libxcb-keysyms libxcb-keysyms1-dev libxcb-image libxcb-image0-dev libxcb-shm libxcb-shm0-dev libxcb-icccm libxcb-icccm4-dev libxcb-sync0-dev libxcb-xfixes0-de libxrender-dev libxcb-shape0-dev
# to enable Qt WebKit
sudo apt-get -yV install gperf bison flex 


###################################
## change default applications
sudo sed -i -e s/eog\.desktop/geeqie.desktop/ /usr/share/applications/defaults.list 
# sudo sed -i -e s/gedit\.desktop/sublime_text.desktop/ /usr/share/applications/defaults.list 
