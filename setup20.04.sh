#!/bin/bash

###################################
## additional repository
# google chrome
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

# typora
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'
# TexStudio
sudo add-apt-repository ppa:sunderme/texstudio

# apply the added repository
sudo apt update

###################################
## primitive tools
# pkg-config
sudo apt -yV install pkg-config 
# enable to use exfat formated drive
sudo apt -yV install exfat-fuse exfat-utils
# vim
sudo apt -yV install vim
# Google chrome
sudo apt install -yV google-chrome-stable
# Nemo file manager 
sudo apt -yV install nemo
# Double Commander
sudo apt -yV install doublecmd-gtk
# sublime-text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt -yV install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update
sudo apt -yV install sublime-text
# typora
sudo apt -yV install typora

###################################
## Communications
# slack (enable japanese input https://cpoint-lab.co.jp/article/202106/20216/)
wget https://downloads.slack-edge.com/releases/linux/4.20.0/prod/x64/slack-desktop-4.20.0-amd64.deb
sudo apt install gdebi
sudo gdebi slack-desktop-4.20.0-amd64.deb

# zoom
sudo apt install -yV gdebi libxcb-xtest0
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb

###################################
## Graphics
# image viewer
wget https://download.xnview.com/XnViewMP-linux-x64.deb
sudo dpkg -i XnViewMP-linux-x64.deb
# gimp
sudo apt -yV install gimp
# Blender
sudo apt -yV install blender
# imagemagick
sudo apt -yV install imagemagick perlmagick
# video
sudo apt -yV install vlc browser-plugin-vlc

###################################
## diff tools
# diff viewer
sudo apt -yV install meld
# diff for pdf
sudo apt -yV install diffpdf

###################################
## for version control
# git, cvs
sudo apt -yV install git cvs


###################################
## C++
# building c/c++
sudo apt -yV install build-essential
# Doxygen
sudo apt -yV install doxygen doxygen-gui graphviz
# CMake
sudo apt -yV install cmake cmake-gui cmake-curses-gui
# Boost
sudo apt -yV install libboost-all-dev
# BLAS & LAPACK
sudo apt -yV install libblas-dev libatlas-dev liblapack-dev libatlas-base-dev
# imagemagick
sudo apt -yV install libmagick++-dev libmagickcore-dev libmagickwand-dev
# CImg
sudo apt -yV install cimg-dev cimg-doc cimg-examples
# TBB
sudo apt -yV install libtbb-dev
# gnuplot
sudo apt -yV install gnuplot
# gfortran 
sudo apt -yV install gfortran

###################################
## Tex
# TeXLive
sudo apt -yV install texlive-full texlive-fonts-recommended texlive-science texlive-lang-cjk texlive-bibtex-extra texlive-fonts-extra biber xdvik-ja

# TeX related applications
sudo apt -yV install texstudio jabref



###################################
## Python
cd ~/Download
wget "https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.21.9712.tar.gz?_ga=2.100343581.2095636819.1633414436-1459220728.1633414436&_gl=1*1vukqqt*_ga*MTQ1OTIyMDcyOC4xNjMzNDE0NDM2*_ga_V0XZL7QHEB*MTYzMzQxNDQ0Ni4xLjEuMTYzMzQxNDQ1NS4w" -O jetbrains_toolbox.tar.gz
tar -xzf jetbrains_toolbox.tar.gz
cd jetbrains-toolbox-1.21.9712
./jetbrains-toolbox
