#!/bin/bash
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-add-repository "deb https://download.sublimetext.com/ apt/stable/"
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo apt update

###################################
## primitive tools
# pkg-config
sudo apt -yV install pkg-config 
# enable to use exfat formated drive
sudo apt -yV install exfat-fuse exfat-utils
# vim
sudo apt -yV install vim
# Nemo file manager 
sudo apt -yV install nemo
# Double Commander
sudo apt -yV install doublecmd-gtk
# sublime-text
sudo apt -yV install sublime-text
# atom
sudo apt -yV install atom

###################################
## Graphics
# image viewer
sudo apt -yV install geeqie
# gimp
sudo apt -yV install gimp
# Blender
sudo apt -yV install blender
# imagemagick
sudo apt -yV install imagemagick perlmagick
# video
sudo apt -yV install vlc browser-plugin-vlc

###################################
## diff tools
# diff viewer
sudo apt -yV install meld
# diff for pdf
sudo apt -yV install diffpdf

###################################
## for version control
# git, cvs
sudo apt -yV install git cvs

###################################
## C++
# building c/c++
sudo apt -yV install build-essential
# Doxygen
sudo apt -yV install doxygen doxygen-gui graphviz
# CMake
sudo apt -yV install cmake cmake-gui cmake-curses-gui
# Boost
sudo apt -yV install libboost-all-dev
# BLAS & LAPACK
sudo apt -yV install libblas-dev libatlas-dev liblapack-dev libatlas-base-dev
# imagemagick
sudo apt -yV install libmagick++-dev libmagickcore-dev libmagickwand-dev
# CImg
sudo apt -yV install cimg-dev cimg-doc cimg-examples
# TBB
sudo apt -yV install libtbb-dev
# SuiteSparse
sudo apt -yV install libsuitesparse-dev
# gnuplot
sudo apt -yV install gnuplot
# gfortran 
sudo apt -yV install gfortran
# Qt creator
sudo apt -yV install qtcreator qt5-default

# TeXLive
sudo apt -yV install texlive-full texlive-fonts-recommended texlive-science texlive-lang-cjk texlive-bibtex-extra biber

# TeX related applications
sudo apt -yV install texstudio jabref
