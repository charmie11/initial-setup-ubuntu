#!/bin/bash

###################################
## Google Chrome
sudo apt -yV install curl
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/googlechrom-keyring.gpg] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
curl -fsSL https://dl.google.com/linux/linux_signing_key.pub | sudo gpg --dearmor -o /usr/share/keyrings/googlechrom-keyring.gpg
sudo apt update
sudo apt install google-chrome-stable

###################################
## primitive tools
# pkg-config
sudo apt -yV install pkg-config 
# enable to use exfat formated drive
sudo apt -yV install exfat-fuse
# vim
sudo apt -yV install vim
# Nemo file manager 
sudo apt -yV install nemo
# Double Commander
sudo apt -yV install doublecmd-gtk
# sublime-text
# see https://tecadmin.net/how-to-install-sublime-text-on-ubuntu-22-04/
sudo apt install gnupg2 wget ca-certificates lsb-release software-properties-common -yV
wget -nc https://download.sublimetext.com/sublimehq-pub.gpg
cat sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-pub.gpg >/dev/null
sudo add-apt-repository "deb https://download.sublimetext.com/ apt/stable/" 
sudo apt update 
sudo apt install sublime-text -yV
# AppImageLauncher
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt update
sudo apt install appimagelauncher

###################################
## Communications
wget https://downloads.slack-edge.com/releases/linux/4.29.149/prod/x64/slack-desktop-4.29.149-amd64.deb
sudo apt install gdebi -yV
sudo gdebi slack-desktop-4.29.149-amd64.deb

# zoom
sudo apt install libgl1-mesa-glx libegl1-mesa libxcb-cursor0 libxcb-xtest0 libxcb-xinerama0
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo dpkg -i zoom_amd64.deb
sudo apt --fix-broken install

###################################
## Graphics
# image viewer
sudo apt -yV install libgdk-pixbuf2.0-0 libopenal1 libxcb-xinerama0
wget https://download.xnview.com/XnViewMP-linux-x64.deb
sudo dpkg -i XnViewMP-linux-x64.deb
sudo apt --fix-broken install
# gimp
sudo apt -yV install gimp
# Blender
sudo apt -yV install blender
# imagemagick
sudo apt -yV install imagemagick perlmagick

###################################
## diff tools
# diff viewer
sudo apt -yV install meld
# diff for pdf
sudo apt -yV install diffpdf

###################################
## for version control
# git, cvs
sudo apt -yV install git cvs


###################################
## C++
# building c/c++
sudo apt -yV install build-essential
# Doxygen
sudo apt -yV install doxygen doxygen-gui graphviz
# CMake
sudo apt -yV install cmake cmake-gui cmake-curses-gui
# Boost
sudo apt -yV install libboost-all-dev
# BLAS & LAPACK
sudo apt -yV install libblas-dev libatlas-dev liblapack-dev
# imagemagick
sudo apt -yV install libmagick++-dev libmagickcore-dev libmagickwand-dev
# CImg
sudo apt -yV install cimg-dev cimg-doc cimg-examples
# TBB
sudo apt -yV install libtbb-dev
# gnuplot
sudo apt -yV install gnuplot
# gfortran 
sudo apt -yV install gfortran

##################################
## Mendeley
sudo mkdir /opt/mendeley && sudo chmod ugo+w /opt/mendeley
cd ~/Download
wget https://static.mendeley.com/bin/desktop/mendeley-reference-manager-2.86.0-x86_64.AppImage
chmod +x mendeley-reference-manager-2.86.0-x86_64.AppImage 


###################################
### Tex
# TeXLive
sudo apt install -yV fonts-ipafont
sudo apt -yV install texlive-full texlive-fonts-recommended texlive-science texlive-lang-cjk texlive-bibtex-extra texlive-fonts-extra biber xdvik-ja

## TeX related applications
sudo apt -yV install texstudio jabref


###################################
## Python
## Anaconda ( Python package manager)
sudo mkdir /opt/anaconda && sudo chmod ugo+w /opt/anaconda
cd ~/Download
wget https://repo.anaconda.com/archive/Anaconda3-2023.03-Linux-x86_64.sh
bash Anaconda3-2023.03-Linux-x86_64.sh
