#!/bin/bash
# Nemo file manager 
sudo add-apt-repository ppa:webupd8team/nemo -y
# Double Commander
sudo add-apt-repository ppa:alexx2000/doublecmd -y
# gimp
sudo add-apt-repository ppa:otto-kesselgulasch/gimp -y

sudo apt update

# sublime-text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update
sudo apt -yV install sublime-text

###################################
## primitive tools
# pkg-config
sudo apt -yV install pkg-config 
# open in terminal
sudo apt -yV install nautilus-open-terminal
# vim
sudo apt -yV install vim
# Nemo file manager 
sudo apt -yV install nemo nemo-fileroller
# Double Commander
sudo apt -yV install doublecmd-gtk

###################################
## Graphics
# image viewer
sudo apt -yV install geeqie
# gimp
sudo apt -yV install gimp
# Blender
sudo apt -yV install blender
# imagemagick
sudo apt -yV install imagemagick perlmagick
# video
sudo apt -yV install vlc browser-plugin-vlc

###################################
## diff tools
# diff viewer
sudo apt -yV install meld
# diff for pdf
sudo apt -yV install diffpdf

###################################
## for version control
# git, cvs
sudo apt -yV install git cvs
# RabbitVCS
sudo apt -yV install rabbitvcs-nautilus rabbitvcs-gedit nemo-rabbitvcs

###################################
## C++
# building c/c++
sudo apt -yV install build-essential
# debugger
sudo apt -yV install valgrind 
# Doxygen
sudo apt -yV install doxygen doxygen-gui graphviz
# CMake
sudo apt -yV install cmake cmake-gui cmake-curses-gui
# Boost
sudo apt -yV install libboost-all-dev
# BLAS & LAPACK
sudo apt -yV install libblas-dev libatlas-dev liblapack-dev libatlas-base-dev
# OpenGL
sudo apt -yV install freeglut3-dev
sudo apt -yV install libglew1.5-dev libglm-dev
# imagemagick
sudo apt -yV install libmagick++-dev libmagickcore-dev libmagickwand-dev
# CImg
sudo apt -yV install cimg-dev cimg-doc cimg-examples
# TBB
sudo apt -yV install libtbb-dev
# SuiteSparse
sudo apt -yV install libsuitesparse-dev
# gnuplot
sudo apt -yV install gnuplot
# pre-requisities for ceres-solver
sudo apt -yV install python-sphinx
# pre-requisities for mlpack
sudo apt -yV install libxml2-dev txt2man
# pre-requisities for libDAI
sudo apt -yV install libgmp-dev swig
# pre-requisities for OpenGM
sudo apt -yV install libhdf5-serial-dev
# pre-requisities for OpenCV
sudo apt -yV install libvtk5-qt4-dev libqt4-dev libqt4-opengl-dev libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant libqt5-*
# Blitz++ for arrays computation
sudo apt -yV install libblitz-doc libblitz0-dev libblitz0ldbl
# 
sudo apt -yV install gfortran

## XML parser for C++
# TinyXML2
sudo apt -yV install libtinyxml-dev
# for xml
sudo apt -yV install libxerces-c-dev xsdcxx

# TeXLive
sudo apt -yV install texlive-full texlive-fonts-recommended texlive-science texlive-lang-cjk texlive-bibtex-extra biber

# JabRef
sudo apt -yV install jabref
